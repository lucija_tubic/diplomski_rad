import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from ast import literal_eval


def get_director_from_data(data):
    for i in data:
        if i['job'] == 'Director':
            return i['name']
    return np.nan


def get_list_genres(data):
    return list(data.split('|'))[:3]


def get_list_features(data):
    if isinstance(data, list):
        names = [i['name'] for i in data]
        # check if more than 3 elements exist. If yes, return only first three. If no, return entire list.
        if len(names) > 3:
            names = names[:3]
        return names
    # return empty list in case of missing/malformed data
    return []


def clean_data(data):
    # convert all strings to lower case and strip names of spaces
    if isinstance(data, list):
        return [str.lower(i.replace(" ", "")) for i in data]
    else:
        # check if director exists. If not, return empty string
        if isinstance(data, str):
            return str.lower(data.replace(" ", ""))
        else:
            return ''


def create_words_soup(data):
    return ' '.join(data['keywords']) + ' ' + ' '.join(data['cast']) + ' ' +\
           data['director'] + ' ' + ' '.join(data['genres'])


def content_based(movie_data):
    movie_data['genres'] = movie_data['genres'].apply(get_list_genres)
    features = ['cast', 'crew', 'keywords']
    for feature in features:
        movie_data[feature] = movie_data[feature].apply(literal_eval)

    movie_data['director'] = movie_data['crew'].apply(get_director_from_data)

    features = ['cast', 'keywords']
    for feature in features:
        movie_data[feature] = movie_data[feature].apply(get_list_features)

    features = ['cast', 'keywords', 'director', 'genres']

    for feature in features:
        movie_data[feature] = movie_data[feature].apply(clean_data)

    movie_data['soup'] = movie_data.apply(create_words_soup, axis=1)
    count = CountVectorizer(stop_words='english')
    count_matrix = count.fit_transform(movie_data['soup'])
    cos_sim = cosine_similarity(count_matrix, count_matrix)
    # movie_data = movie_data.reset_index()
    
    return cos_sim
