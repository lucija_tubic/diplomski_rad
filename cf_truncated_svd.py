from sklearn.decomposition import TruncatedSVD as tSVD
from copy import deepcopy


def create_avg_table(org_table):
    avg_table = []
    user_averages = []
    for row in org_table:
        br = 0.0
        row_sum = 0.0
        for x in row:
            if x != 0:
                row_sum += x
                br += 1.0
        if br != 0:
            avg = row_sum / br
            user_averages.append(avg)
            new_row = []
            for m in row:
                if m == 0:
                    new_row.append(0)  # no_rating
                else:
                    new_row.append(m - avg)
            avg_table.append(new_row)
        else:
            user_averages.append(0)
            avg_table.append([0] * len(row))  # no_rating
    return avg_table, user_averages


def clip_values(x):
    if x < 0.5:
        return 0.5
    if x > 5:
        return 5
    return x


def cf_truncated_svd(table, most_popular_movies_indices, most_active_users_indices,
                     movies_set, users_set, km=2, k=15, p=0.05):
    n_movies = len(movies_set)
    n_users = len(users_set)
    # print("check users, movies ", n_users, n_movies)

    Nu = int(p * n_users)
    Nm = int(p * n_movies)
    # print("Nu=", Nu, "  Nm=", Nm)
    # print('km=', km, 'k=', k)

    averages_table, averages = create_avg_table(table)
    tmp_table = deepcopy(table)

    M = [[] for _ in range(Nu)]
    averages_M = []
    for i in range(Nu):
        for j in range(Nm):
            M[i].append(averages_table[most_active_users_indices[i]][most_popular_movies_indices[j]])
            averages_M.append(averages[most_active_users_indices[i]])

    model_M = tSVD(n_components=km)
    W = model_M.fit_transform(M)
    reconstructed_M = model_M.inverse_transform(W)

    for i in range(Nu):
        for j in range(Nm):
            if M[i][j] == 0:
                x = reconstructed_M[i][j] + averages_M[i]
                tmp_table[most_active_users_indices[i]][most_popular_movies_indices[j]] = clip_values(x)

    averages_table_tmp, averages_tmp = create_avg_table(tmp_table)
    model = tSVD(n_components=k, n_iter=10)
    W = model.fit_transform(averages_table_tmp)
    reconstructed = model.inverse_transform(W)

    return reconstructed, averages
