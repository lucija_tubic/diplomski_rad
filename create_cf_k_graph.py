import math
from cf_truncated_svd import *
from hybrid import load_data, create_table, split_data
import create_statistics
import matplotlib.pyplot as plt


def cf_calculate_rmse(test_data, movies, users_set, reconstructed, averages):
    err = 0
    test_values = test_data.values
    for val in test_values:
        movie_id = movies.index(val[0])
        user_id = users_set.index(val[3])
        rating = val[4]

        # CF_prediction
        x = reconstructed[user_id][movie_id] + averages[user_id]
        prediction = clip_values(x)

        # error
        err += math.pow(prediction - rating, 2)

    return math.sqrt(err / len(test_data))


def spin(n_iter, movie_user_data, movies_set, users_set, p):
    rmse_list = []
    n_movies = len(movies_set)

    for i in range(n_iter):
        test_data, train_data = split_data(movie_user_data)

        train_table, most_popular_movies_indices, most_active_users_indices = create_table(train_data,
                                                                                           movies_set, users_set)
        reconstructed, averages = cf_truncated_svd(train_table, most_popular_movies_indices, most_active_users_indices,
                                                   movies_set, users_set,
                                                   km=1+math.ceil(p*0.05*n_movies), k=math.ceil(p*n_movies))

        rmse = cf_calculate_rmse(test_data, movies_set, users_set, reconstructed, averages)

        # print("rmse ", rmse)
        rmse_list.append(rmse)

    return sum(rmse_list) / len(rmse_list)


def main():
    n = 10
    movie_data, movie_user_data, movies_set, users_set = load_data()

    cf_list = [0.0005 * i for i in range(1, 15)]
    # cont_cf_list = [0.1 * i for i in range(11)]

    cf_plot_list = []
    for xp in cf_list:
        err = spin(n, movie_user_data, movies_set, users_set, p=xp)
        print('p za K = %.5f' % xp, ', rmse =', "%g" % err)
        cf_plot_list.append((xp, err))

    mode = 'ro'
    fig_ab = plt.figure()
    plt.xlabel('coef for K')
    plt.ylabel('RMSE')
    for (x, y) in cf_plot_list:
        plt.plot(x, y, mode, label="n=" + str(n))

    fig_ab.savefig('plots/graph_cf_n' + str(n) + '.png')


if __name__ == "__main__":

    create_statistics.run_and_create_stats('main()', 'stats/create_cf_graph.txt')
