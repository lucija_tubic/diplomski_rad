from cf_truncated_svd import cf_truncated_svd
from content_based import content_based
from easygui import choicebox, codebox
from hybrid import load_data, create_table, clip_values
from decimal import Decimal, ROUND_HALF_UP


if __name__ == "__main__":
    movie_data, movie_ratings_data, movies_set, movie_names, users_set = load_data(return_movie_names=True)
    n_movies = len(movies_set)
    gui_users_list = ['{: >3}'.format(int(user)) for user in users_set]

    content_movies = [val[0] for val in movie_data.values]

    content_similarity = content_based(movie_data)

    table, most_popular_movies_indices, most_active_users_indices = create_table(movie_ratings_data,
                                                                                 movies_set, users_set)

    reconstructed, averages = cf_truncated_svd(table, most_popular_movies_indices, most_active_users_indices,
                                               movies_set, users_set)

    a = 0.1
    b = 1 - a
    msg = "To exit press Cancel.\n\nChose a user."
    title = "User Recommendations"
    while True:
        choice = choicebox(msg, title, sorted(list(gui_users_list)))
        if choice:
            print("Odabran je korisnik ", choice)
            selected_user_id = users_set.index(int(choice))
            not_viewed_movies_predictions = {}
            user_row = table[selected_user_id]
            user_reconstructed = reconstructed[selected_user_id]
            user_average = averages[selected_user_id]
            for movie_id in range(n_movies):
                if user_row[movie_id] == 0:
                    x_1 = 0
                    if movies_set[movie_id] in content_movies:
                        content_movie_id = content_movies.index(movies_set[movie_id])
                        g = 0
                        d = 0
                        for i in range(len(content_movies)):
                            if table[selected_user_id][i] != 0:
                                sim = content_similarity[content_movie_id][i]
                                if sim > 0:
                                    g += sim * table[selected_user_id][i]
                                    d += sim
                        if d != 0:
                            x_1 = g / d

                    x_2 = user_reconstructed[movie_id] + user_average

                    not_viewed_movies_predictions[movie_id] = clip_values(a*x_1 + b*x_2)

            recommendations_list = ['{:10}{:30}'.format('Score', 'Titles') + "\n", "\n"]
            for k, v in sorted(not_viewed_movies_predictions.items(), key=lambda kv: -kv[1])[:10]:
                recommendations_list.append('{:10}{:40}'.format(
                    str(Decimal(Decimal(v).quantize(Decimal('.001'), rounding=ROUND_HALF_UP))),
                    movie_names[movie_names['movieId'] == movies_set[k]].values[0][1]) + "\n")
            if len(recommendations_list) > 2:
                codebox("Recommendations for user  " + choice, "Show Recommendations", recommendations_list)
            else:
                codebox("Recommendations for user  " + choice, "Show Recommendations", "No recommendations found.")
        else:
            print("\nTerminated.")
            break
