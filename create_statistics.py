import cProfile
import pstats


def run_and_create_stats(func, file):
    pr = cProfile.Profile()
    pr.run(func)
    ps = pstats.Stats(pr, stream=open(file, 'w')).sort_stats('cumtime')
    ps.print_stats('diplomski_rad')
