import pandas as pd
import math
from random import randrange
from cf_truncated_svd import cf_truncated_svd, clip_values
from content_based import content_based
import create_statistics

file = "data"


def load_data(ratings_file=file+'/ratings.csv', movies_file=file+'/movies.csv',
              links_file=file+'/links.csv', credits_file=file+'/credits.csv',
              keywords_file=file+'/keywords.csv', return_movie_names=False):
    ratings_data = pd.read_csv(ratings_file)
    movie_names = pd.read_csv(movies_file)
    links = pd.read_csv(links_file)
    movie_credits = pd.read_csv(credits_file)
    keywords = pd.read_csv(keywords_file)

    movie_ratings_data = pd.merge(movie_names, ratings_data, on='movieId')

    movie_data = pd.merge(movie_names, links, on='movieId')
    movie_data = movie_data.merge(movie_credits, left_on='tmdbId', right_on='id')
    movie_data = movie_data.merge(keywords, how='left', on='id')
    # print("movie data for content based", movie_data.shape)

    movies_set = [val[0] for val in movie_names.values]
    users_set = [val[0] for val in movie_ratings_data.groupby('userId')]

    n_movies = len(movies_set)
    n_users = len(users_set)
    n_ratings = len(movie_ratings_data)
    print("Number of movies: %10d" % n_movies)
    print("Number of users: %10d" % n_users)
    print("Number of ratings: %10d" % n_ratings, "\n")

    if return_movie_names:
        return movie_data, movie_ratings_data, movies_set, movie_names, users_set

    return movie_data, movie_ratings_data, movies_set, users_set


def create_table(data, movies_set, users_set):
    n_movies = len(movies_set)
    n_users = len(users_set)

    table = []
    for i in range(n_users):
        table.append([0] * n_movies)

    movie_popularity = [0] * n_movies
    user_activity = [0] * n_users

    values = data.values
    for val in values:
        movie_id = movies_set.index(val[0])
        user_id = users_set.index(val[3])
        rating = val[4]

        table[user_id][movie_id] = rating  # druga [] je user-1
        movie_popularity[movie_id] += 1
        user_activity[user_id] += 1

    most_popular_movies_indices = sorted(range(len(movie_popularity)), key=lambda k: -movie_popularity[k])
    most_active_users_indices = sorted(range(len(user_activity)), key=lambda k: -user_activity[k])

    return table, most_popular_movies_indices, most_active_users_indices


def split_data(data, p=0.05):
    n_ratings = len(data)
    T = int(p * n_ratings)
    test_index_list = []

    for i in range(T):
        r = randrange(0, n_ratings)
        while r in test_index_list:
            r = randrange(0, n_ratings)
        test_index_list.append(r)

    test_data = data[data.index.isin(test_index_list)]
    train_data = data.drop(test_index_list)
    # print('test:', len(test_data), 'train', len(train_data))

    return test_data, train_data


def calculate_rmse(test_data, movies_set, content_movies, users_set, content_similarity, table, reconstructed, averages,
                   a=0.1):
    b = 1 - a   # a is content factor, b is cf factor
    err = 0
    test_values = test_data.values
    for val in test_values:
        movie_id = movies_set.index(val[0])
        user_id = users_set.index(val[3])
        rating = val[4]

        # content based prediction
        x_1 = 0
        if val[0] in content_movies:
            content_movie_id = content_movies.index(val[0])
            g = 0
            d = 0
            for i in range(len(content_movies)):
                if table[user_id][i] != 0:
                    sim = content_similarity[content_movie_id][i]
                    if sim > 0:
                        g += sim * table[user_id][i]
                        d += sim
            if d != 0:
                x_1 = g/d

        # CF_prediction
        x_2 = reconstructed[user_id][movie_id] + averages[user_id]

        # error
        prediction = clip_values(a*x_1 + b*x_2)
        err += math.pow(prediction - rating, 2)

    return math.sqrt(err / len(test_data))


def main():
    movie_data, movie_ratings_data, movies_set, users_set = load_data()

    content_movies = [val[0] for val in movie_data.values]
    # print("content movies", len(content_movies))

    content_similarity = content_based(movie_data)

    test_data, train_data = split_data(movie_ratings_data)
    train_table, most_popular_movies_indices, most_active_users_indices = create_table(train_data,
                                                                                       movies_set, users_set)

    reconstructed, averages = cf_truncated_svd(train_table, most_popular_movies_indices, most_active_users_indices,
                                               movies_set, users_set)

    rmse = calculate_rmse(test_data, movies_set, content_movies, users_set, content_similarity,
                          train_table, reconstructed, averages)

    print("RMSE", rmse)


if __name__ == "__main__":

    create_statistics.run_and_create_stats('main()', 'stats/hybrid.txt')
