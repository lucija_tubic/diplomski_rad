from cf_truncated_svd import *
from content_based import *
from hybrid import load_data, split_data, create_table, calculate_rmse
import matplotlib.pyplot as plt
import create_statistics


def spin(n_iter, movie_user_data, movies_set, users_set, content_movies, content_similarity, a=0.05):
    rmse_list = []

    for i in range(n_iter):
        test_data, train_data = split_data(movie_user_data)
        train_table, most_popular_movies_indices, most_active_users_indices = create_table(train_data,
                                                                                           movies_set, users_set)

        reconstructed, averages = cf_truncated_svd(train_table, most_popular_movies_indices, most_active_users_indices,
                                                   movies_set, users_set)

        rmse = calculate_rmse(test_data, movies_set, content_movies, users_set, content_similarity,
                              train_table, reconstructed, averages, a)
        # print("rmse ", rmse)
        rmse_list.append(rmse)

    return sum(rmse_list) / len(rmse_list)


def main():
    n = 10  # 5
    movie_data, movie_user_data, movies_set, users_set = load_data()

    content_movies = [val[0] for val in movie_data.values]

    content_similarity = content_based(movie_data)

    # cont_cf_list = [0.125 * i for i in range(9)]
    cont_cf_list = [0.05 * i for i in range(21)]

    ab_plot_list = []
    for xa in cont_cf_list:
        err = spin(n, movie_user_data, movies_set, users_set, content_movies, content_similarity, a=xa)
        print('content coef = %.3f' % xa, ', rmse =', "%g" % err)
        ab_plot_list.append((xa, err))

    mode = 'ro'
    fig_ab = plt.figure()
    plt.xlabel('content coef')
    plt.ylabel('RMSE')
    for (x, y) in ab_plot_list:
        plt.plot(x, y, mode, label="n=" + str(n))

    fig_ab.savefig('plots/graph_ab_' + str(n) + '.png')


if __name__ == "__main__":

    create_statistics.run_and_create_stats('main()', 'stats/create_a_b_graph.txt')
